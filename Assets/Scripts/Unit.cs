﻿using System;
using UnityEngine;

namespace RobertElf
{
    public class Unit:MonoBehaviour
    {
        public int TeamId { get; private set; }
        public float CopyDelay { get; private set; }

        private const float DEFAULT_DELAY = 1f;

        public void SetIndex(int teamIndex)
        {
            TeamId = teamIndex;
            SetCopyTimout();
        }


        public void SetCopyTimout()
        {
            CopyDelay = DEFAULT_DELAY;
        }
        private void FixedUpdate()
        {
            if (CopyDelay > 0)
            {
                CopyDelay -= Time.fixedDeltaTime;
            }
        }
    }
}