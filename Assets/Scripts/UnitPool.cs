﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace RobertElf
{
    public class UnitPool:MonoBehaviour
    {
        [SerializeField] private TeamConfigs teamConfigs;
        
        
        private List<Unit> units = new List<Unit>();
        public event Action<Unit> OnSpawnUnit;        
        public event Action<Unit> OnDespawnUnit;
        
        private void Start()
        {
            Unit[] initialUnits = FindObjectsOfType<Unit>();
            units.AddRange(initialUnits);

            foreach (var unit in units)
            {
                unit.gameObject.SetActive(false);                
            }
        }

        public Option<Unit, None> RetrieveUnit(int teamIndex)
        {
            if (units.Count == 0)
            {
                return new Option<Unit, None>(new None());
            }
            
            Unit retrievedUnit = units[^1];
            units.RemoveAt(units.Count-1);
            retrievedUnit.gameObject.SetActive(true);
            retrievedUnit.GetComponent<MeshRenderer>().SetMaterials(teamConfigs.GetConfig(teamIndex).Materials);
            retrievedUnit.SetIndex(teamIndex);
            
            OnSpawnUnit?.Invoke(retrievedUnit);
            return new Option<Unit, None>(retrievedUnit);
        }
        
        public void ReturnUnit(Unit unit)
        {
            unit.gameObject.SetActive(false);
            units.Add(unit);
            OnDespawnUnit?.Invoke(unit);
        }
    }
}