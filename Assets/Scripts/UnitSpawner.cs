﻿using UnityEngine;

namespace RobertElf
{
    public class UnitSpawner : MonoBehaviour
    {
        [SerializeField] private UnitPool unitPool;
        public float TimeInterval = 3; //seconds
        [SerializeField] private Transform targetSpawnPosition;
        [SerializeField] private int teamIndex;

        private float timeSinceLastSpawn = 0;

        private void Start()
        {
            Spawn();
        } 
        private void FixedUpdate()
        {
            timeSinceLastSpawn += Time.fixedDeltaTime;

            if (timeSinceLastSpawn > TimeInterval)
            {
                timeSinceLastSpawn -= TimeInterval;
                Spawn();
            }
        }

        private void Spawn()
        {
            var retrievedUnitOption = unitPool.RetrieveUnit(teamIndex);

            retrievedUnitOption.Match(
                retrievedUnit => { retrievedUnit.transform.position = targetSpawnPosition.position; },
                none => { });
        }
    }
}