using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace RobertElf
{
    public class WalkRandomly : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent navMeshAgent;
        [SerializeField] private float nextMovementRange = 10;
        [SerializeField] private float minTimeDelay = 0.5f;
        [SerializeField] private float maxTimeDelay = 3f;
        
        private NavMeshHit meshHit;
        Vector3 RandomTranslation()
        {
            return new Vector3(Random.Range(-nextMovementRange, nextMovementRange), 0,
                Random.Range(-nextMovementRange, nextMovementRange));
        }


        private void OnEnable()
        {
            SetRandomDestination();
            StartCoroutine(RandomMovementCoroutine());
        }

        private IEnumerator RandomMovementCoroutine()
        {

            while (true)
            {
                yield return null;
                yield return new WaitForSeconds(Random.Range(minTimeDelay, maxTimeDelay));

                SetRandomDestination();
            }
        }

        private void SetRandomDestination()
        {
            Vector3 targetPos = transform.position + RandomTranslation();

            bool targetHit = NavMesh.SamplePosition(targetPos, out meshHit, 1000, 1);

            if (targetHit)
            {
                navMeshAgent.SetDestination(meshHit.position);
            }
        }
        
    }
}