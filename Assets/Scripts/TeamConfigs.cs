﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RobertElf
{
    [Serializable]
    public struct TeamConfig
    {
        [SerializeField]
        private List<Material> materials;

        public List<Material> Materials => materials;
    }

    [CreateAssetMenu(fileName = "TeamConfig", menuName = "Configs/TeamConfig" +
                                                         "", order = 0)]
    public class TeamConfigs : ScriptableObject
    {
        [SerializeField]
        private List<TeamConfig> configs;

        public TeamConfig GetConfig(int teamIndex)
        {
            return configs[teamIndex];
        }
    }
}