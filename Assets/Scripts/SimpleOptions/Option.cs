﻿using System;

namespace RobertElf
{
    public struct Option<T1, T2>
    {
        private T1 obj1;
        private T2 obj2;

        private int objIndex;
        
        public Option(T1 obj1)
        {
            this.obj1 = obj1;
            obj2 = default;
            
            objIndex = 1;
        }
        
        public Option(T2 obj2)
        {
            obj1 = default;
            this.obj2 = obj2;
            
            objIndex = 2;
        }

        public void Match(Action<T1> matchT1, Action<T2> matchT2)
        {
            switch (objIndex)
            {
                case 1:
                    matchT1?.Invoke(obj1);
                    break;
                case 2:
                    matchT2?.Invoke(obj2);
                    break;
            }
        }
        
    }
}