﻿using System;
using TMPro;
using UnityEngine;

namespace RobertElf
{
    public class SpawnerTimeController : MonoBehaviour
    {
        [SerializeField] private TMP_InputField inputField;
        [SerializeField] private UnitSpawner unitSpawner;

        private void Start()
        {
            inputField.text = $"{unitSpawner.TimeInterval}";
        }

        public void OnEndEdit()
        {
            if (float.TryParse(inputField.text, out float value))
            {
                unitSpawner.TimeInterval = value;
            }
            else
            {
                inputField.text = $"{unitSpawner.TimeInterval}";
            }
        }
    }
}