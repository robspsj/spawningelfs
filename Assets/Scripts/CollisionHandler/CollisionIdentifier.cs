﻿using System;
using UnityEngine;

namespace RobertElf
{
    [RequireComponent(typeof(Unit))]
    public class CollisionIdentifier:MonoBehaviour
    {
        private CollisionHandler collisionHandler;
        [SerializeField] private Collider colliderCache;

        public void RegisterCollisionHandler(CollisionHandler collisionHandler)
        {
            this.collisionHandler = collisionHandler;
        }
        
        public void OnTriggerEnter(Collider other)
        {
            if (collisionHandler == null)
            {
                return;
            }
            
            collisionHandler.RegisterCollision(colliderCache, other);
        }
    }
}