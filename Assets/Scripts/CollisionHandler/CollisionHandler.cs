﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RobertElf
{
    public class CollisionHandler:MonoBehaviour
    {
        [SerializeField] private UnitPool unitPool;
        
        private Dictionary<Collider, Collider> detectedCollision = new();
        private Collection<Collider> collidersProcessed = new();

        private void Awake()
        {            
            Unit[] initialUnits = FindObjectsOfType<Unit>();

            foreach (Unit unit in initialUnits)
            {
                unit.GetComponent<CollisionIdentifier>().RegisterCollisionHandler(this);
            }
        }

        // private void OnEnable()
        // {
        //     unitPool.OnSpawnUnit += UnitPoolOnOnSpawnUnit;
        //     unitPool.OnDespawnUnit += UnitPoolOnOnDespawnUnit;
        // }
        //
        // private void OnDisable()
        // {
        //     unitPool.OnSpawnUnit -= UnitPoolOnOnSpawnUnit;
        //     unitPool.OnDespawnUnit -= UnitPoolOnOnDespawnUnit;
        // }
        //
        // private void UnitPoolOnOnDespawnUnit(Unit unit)
        // {
        //     unitByCollider.Add(unit.GetComponent<Collider>(), unit);
        // }
        //
        // private void UnitPoolOnOnSpawnUnit(Unit unit)
        // {
        //     unitByCollider.Remove(unit.GetComponent<Collider>());
        // }

        public void RegisterCollision(Collider colliderA, Collider colliderB)
        {
            if (collidersProcessed.Contains(colliderA) || collidersProcessed.Contains(colliderB))
            {
                return;
            }

            collidersProcessed.Add(colliderA);
            collidersProcessed.Add(colliderB);
            
            detectedCollision.Add(colliderA, colliderB);

        }
        
        private void FixedUpdate()
        {
            foreach ((Collider colliderIdA, Collider colliderIdB) in detectedCollision)
            {
                Unit unitA = colliderIdA.GetComponent<Unit>();
                Unit unitB = colliderIdB.GetComponent<Unit>();

                if (unitA.CopyDelay > 0 || unitB.CopyDelay > 0)
                {
                    continue;
                }
                
                if (unitA.TeamId == unitB.TeamId)
                {
                    unitA.SetCopyTimout();
                    unitB.SetCopyTimout();
                    
                    var retrievedUnitOption = unitPool.RetrieveUnit(unitA.TeamId);
                    retrievedUnitOption.Match(
                        retrievedUnit =>
                        {
                            var randomAngle = Random.Range(0, 2 * Mathf.PI); 
                            retrievedUnit.transform.position = (colliderIdA.transform.position + colliderIdB.transform.position)*0.5f;
                        },
                        none => {});
                }
                else
                {
                    unitPool.ReturnUnit(unitA);
                    unitPool.ReturnUnit(unitB);
                }
            }
            
            collidersProcessed.Clear();
            detectedCollision.Clear();
        }
    }
}