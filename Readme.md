﻿# Implementation Log

## Checkpoint 1: Basic Navigation

### Implementation
So my first priority was to get a few basic systems working, the idea was to just create a simple map and have 
a few units walking in it with navMeshAgent to validate the approach, also doing a initial profiling
in the device to have a baseline for performance.

My initial approach to get a destination point was to just get a random point inside a square centered around the unit
and getting the nearest point inside to the target and haave the unit setting its destination in fixed intervals.

### analysis
With a single unit in map i was measuring a frame time around 4ms and a total memory of around 105Mb (altough a good
chunk of it was due to the profiller)

With about 500 units the frame time was around 11ms (most of the time in pathfinding internal calculations)
the memory  went up very slightly most is still used by ther profiler

The frame time with 1800 units is about 35 ms a little bellow 60fps so it is above the limit with this approach,
the target with this implementation is to keep the unit limit as close to this as possible

## Checkpoint 2: Capturing Collisions

### implementation

I started changing a little the shape of the map and camera so the experience would  be fully visible in 
mobile portrait mode in most reasonable aspect ratios.

Moving on to collision detection, for this also i will use unity collisors to detect when two elfs meet
each other, what IO do To add a kynematic rigid body to each elf, and detect collisions between them, its important to 
test in this stage the impact of performance of lots of objects hitting each other; 

### analysis
First I'm only interested in capturing the collisions, with about 1400 units the frame time is about 35 ms, so the
experience is still very responsive, in this case i have to take care to avoid allocating memmory in the collision 
since there are a lot of collisions happening on every frame

## Checkpoint 3: Spawners

### Implementation

Now I will start working in the spawners, since there are going to be a lot elfs appearing and dissapearing it is safe
to assume that instantiating and destroying the objects would have a big impact in the performance of the experience, so
I will start using a Unit pool as an implementation

In this case i implemented a very simple pool that don't deal with instantiation of units when it is empty, it just
enable and disable game objects and return them, a small implmentation detail is the use of an Option<GameObject,None>
as return this beahve as a very implicit indicator that the Pool may in fact not return an object and this case must be 
treated

there is some improvement to be done for units that have just been instantiated so they start walking right away but 
this improvement will be done in another time

## Checkpoint 4: Handling Collisions

### Implementation

To make the responsibilities distinct between elements in the unit collider component i will only capture the collisions,
another object will handle the results of those collisions after the fact, this will make it easier to make sure theat 
the collisions will "behave" properly, for exemplo, an elf will only handle a single collision per simulation step


Also add option to edit spawn speed